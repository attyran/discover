package com.example.discover.model;

import com.google.gson.annotations.SerializedName;

public class Restaurant {
    @SerializedName("id")
    private int id;

    @SerializedName("cover_img_url")
    private String cover_img_url;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("status")
    private String status;

    @SerializedName("phone_number")
    private String phone_number;

    @SerializedName("delivery_fee")
    private int delivery_fee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCoverImgUrl() {
        return cover_img_url;
    }

    public void setCoverImgUrl(String cover_img_url) {
        this.cover_img_url = cover_img_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getDeliveryFee() {
        return delivery_fee;
    }

    public void setDeliveryFee(int delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public String getPhoneNumber() {
        return phone_number;
    }

    public void setPhoneNumber(String phone_number) {
        this.phone_number = phone_number;
    }
}
