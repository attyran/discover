package com.example.discover.model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DiscoverApi {
    @GET("restaurant/?&offeset=0&limit=50")
    Call<List<Restaurant>> getNearbyRestaurants(@Query("lat") double lat, @Query("lng") double lng);

    @GET("restaurant/{id}")
    Call<Restaurant> getRestaurant(@Path("id") int id);
}
