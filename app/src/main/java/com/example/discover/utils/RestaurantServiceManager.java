package com.example.discover.utils;

import com.example.discover.model.DiscoverApi;
import com.example.discover.model.Restaurant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestaurantServiceManager {
    private static final String BASE_URL = "https://api.doordash.com/v2/";
    private static final String TAG = "RestaurantViewModel";

    private DiscoverApi api;

    public RestaurantServiceManager() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(DiscoverApi.class);
    }

    public void getNearbyRestaurants(double lat, double lon, Callback<List<Restaurant>> callback) {
        Call<List<Restaurant>> call = api.getNearbyRestaurants(lat, lon);
        call.enqueue(callback);
    }

    public void getRestaurantInfo(int id, Callback<Restaurant> callback) {
        Call<Restaurant> call = api.getRestaurant(id);
        call.enqueue(callback);
    }
}
