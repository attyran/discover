package com.example.discover.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.discover.model.Restaurant;
import com.example.discover.viewmodel.Interactor;
import com.example.discover.R;
import com.example.discover.viewmodel.RestaurantAdapter;
import com.example.discover.databinding.RestaurantListBinding;
import com.example.discover.viewmodel.RestaurantViewModel;

public class NearbyRestaurantsFragment extends Fragment implements Interactor {
    private RestaurantListBinding binding;
    private RestaurantViewModel mRestaurantsViewModel;
    private RestaurantAdapter mAdapter;

    public NearbyRestaurantsFragment() {
        // Required empty public constructor
    }

    public static NearbyRestaurantsFragment newInstance() {
        return new NearbyRestaurantsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_nearyby_restaurants, container, false);

        mRestaurantsViewModel = new RestaurantViewModel(getContext());
        binding.setRestaurantsViewModel(mRestaurantsViewModel);
        binding.recyclerviewSearchResults.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerviewSearchResults.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        mAdapter = new RestaurantAdapter(getContext(), this);
        binding.recyclerviewSearchResults.setAdapter(mAdapter);

        mRestaurantsViewModel.getNearbyRestaurants(37.422740, -122.139956);

        return binding.getRoot();
    }

    @Override
    public void showRestaurantDetails(Restaurant restaurant) {
        FragmentManager fm = getFragmentManager();
        if (fm != null) {
            fm.beginTransaction()
                    .replace(R.id.container, RestaurantDetailsFragment.newInstance(restaurant), "detail")
                    .addToBackStack(null)
                    .commit();
        }
    }
}
