package com.example.discover.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.discover.R;
import com.example.discover.model.Restaurant;
import com.example.discover.viewmodel.DetailInteractor;
import com.example.discover.viewmodel.RestaurantDetailViewModel;
import com.squareup.picasso.Picasso;


public class RestaurantDetailsFragment extends Fragment implements DetailInteractor {
    private ImageView mDetailImage;
    private TextView mDetailPhone;
    private RestaurantDetailViewModel mRestaurantDetailVM;
    private Restaurant mRestaurant;

    public RestaurantDetailsFragment() {
    }

    public static RestaurantDetailsFragment newInstance(Restaurant restaurant) {
        RestaurantDetailsFragment fragment = new RestaurantDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setRestaurant(restaurant);
        return fragment;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.mRestaurant = restaurant;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_restaurant_details, container, false);

        this.mRestaurantDetailVM = new RestaurantDetailViewModel(v.getContext(), this.mRestaurant, this);

        mDetailImage = v.findViewById(R.id.detail_image);
        mDetailPhone = v.findViewById(R.id.detail_phone);

        mRestaurantDetailVM.getRestaurantInfo();

        return v;
    }

    @Override
    public void setDetails(Restaurant restaurant) {
        this.mDetailPhone.setText(String.format(this.mDetailPhone.getText().toString(), restaurant.getPhoneNumber()));

        Picasso.with(getContext())
                .load(restaurant.getCoverImgUrl())
                .into(this.mDetailImage);
    }
}
