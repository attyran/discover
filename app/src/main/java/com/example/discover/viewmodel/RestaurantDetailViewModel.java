package com.example.discover.viewmodel;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.discover.model.Restaurant;
import com.example.discover.utils.RestaurantServiceManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantDetailViewModel {
    private static final String TAG = "RestaurantDetailVM";

    private final Context mContext;
    private Restaurant mRestaurant;
    private DetailInteractor mDetailInteractor;
    private RestaurantServiceManager mRestaurantServiceManager;

    public RestaurantDetailViewModel(@NonNull Context context, Restaurant restaurant, DetailInteractor detailInteractor) {
        this.mContext = context;
        this.mRestaurant = restaurant;
        this.mDetailInteractor = detailInteractor;
        this.mRestaurantServiceManager = new RestaurantServiceManager();
    }

    public void getRestaurantInfo() {
        Callback<Restaurant> callback = new Callback<Restaurant>() {
            @Override
            public void onResponse(Call<Restaurant> call, Response<Restaurant> response) {
                Log.d(TAG, "onResponse");
                Restaurant restaurant = response.body();
                mDetailInteractor.setDetails(restaurant);
            }

            @Override
            public void onFailure(Call<Restaurant> call, Throwable t) {
                Log.e(TAG, "getRestaurantInfo call failed!");
            }
        };

        mRestaurantServiceManager.getRestaurantInfo(mRestaurant.getId(), callback);
    }
}
