package com.example.discover.viewmodel;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;

import com.example.discover.model.Restaurant;

import java.util.List;

public class RestaurantListBinding {
    @BindingAdapter("restaurants")
    public static void setRestaurants(RecyclerView recyclerView, List<Restaurant> restaurants) {
        RecyclerView.Adapter adapter = recyclerView.getAdapter();

        if (adapter != null && adapter instanceof RestaurantAdapter) {
            ((RestaurantAdapter) adapter).setRestaurants(restaurants);
        } else {
            throw new IllegalStateException("RecyclerView either has no adapter set or the "
                    + "adapter isn't of type RestaurantAdapter");
        }
    }
}
