package com.example.discover.viewmodel;

import com.example.discover.model.Restaurant;

/**
 * Restaurant item interface
 */
public interface Interactor {
    void showRestaurantDetails(Restaurant restaurant);
}
