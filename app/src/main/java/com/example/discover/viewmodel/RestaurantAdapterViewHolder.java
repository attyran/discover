package com.example.discover.viewmodel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.discover.R;
import com.example.discover.model.Restaurant;

import java.util.List;

public class RestaurantAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    final ImageView mImage;
    final TextView mName;
    final TextView mDescription;
    final TextView mStatus;

    private Context mContext;

    private List<Restaurant> mRestaurants;

    private Interactor mInteractor;

    public RestaurantAdapterViewHolder(View itemView, Context context, List<Restaurant> restaurants, Interactor interactor) {
        super(itemView);
        mImage = itemView.findViewById(R.id.restaurant_image);
        mName = itemView.findViewById(R.id.restaurant_name);
        mDescription = itemView.findViewById(R.id.restaurant_description);
        mStatus = itemView.findViewById(R.id.restaurant_status);

        this.mContext = context;

        this.mRestaurants = restaurants;

        this.mInteractor = interactor;

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int position = getAdapterPosition();
        Restaurant restaurant = mRestaurants.get(position);
        mInteractor.showRestaurantDetails(restaurant);
    }

}
