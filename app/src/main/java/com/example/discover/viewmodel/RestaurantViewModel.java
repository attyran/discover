package com.example.discover.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.discover.model.Restaurant;
import com.example.discover.utils.RestaurantServiceManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantViewModel extends BaseObservable {
    public final ObservableList<Restaurant> restaurants = new ObservableArrayList<>();
    private final String TAG = "RestaurantVM";

    private final Context mContext;
    private RestaurantServiceManager mRestaurantServiceManager;

    public RestaurantViewModel(@NonNull Context context) {
        this.mContext = context;
        mRestaurantServiceManager = new RestaurantServiceManager();
    }

    public void getNearbyRestaurants(double lat, double lon) {
        Callback<List<Restaurant>> callback = new Callback<List<Restaurant>>() {
            @Override
            public void onResponse(Call<List<Restaurant>> call, Response<List<Restaurant>> response) {
                Log.d(TAG, "onResponse");
                if (response.isSuccessful()) {
                    restaurants.clear();
                    restaurants.addAll(response.body());
                } else {
                    Log.e(TAG, "Query failed!");
                }
            }

            @Override
            public void onFailure(Call<List<Restaurant>> call, Throwable t) {
                Log.e(TAG, "Query failed!");
            }
        };

        mRestaurantServiceManager.getNearbyRestaurants(lat, lon, callback);
    }
}
