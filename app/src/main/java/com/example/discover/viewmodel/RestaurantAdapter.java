package com.example.discover.viewmodel;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.discover.R;
import com.example.discover.model.Restaurant;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapterViewHolder> {
    private List<Restaurant> mRestaurants;
    private Context mContext;
    private Interactor mInteractor;

    public RestaurantAdapter(Context context, Interactor interactor) {
        this.mContext = context;
        this.mInteractor = interactor;
    }

    public void setRestaurants(@NonNull List<Restaurant> movies) {
        this.mRestaurants = movies;
        notifyDataSetChanged();
    }

    @Override
    public RestaurantAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.restaurant_rv_item, parent, false);

        RestaurantAdapterViewHolder holder = new RestaurantAdapterViewHolder(view, mContext, this.mRestaurants, this.mInteractor);

        return holder;
    }

    @Override
    public void onBindViewHolder(RestaurantAdapterViewHolder holder, int position) {
        Restaurant restaurant = mRestaurants.get(position);
        Picasso.with(mContext)
                .load(restaurant.getCoverImgUrl())
                .into(holder.mImage);

        holder.mName.setText(restaurant.getName());

        holder.mDescription.setText(restaurant.getDescription());

        holder.mStatus.setText(restaurant.getStatus());
    }

    @Override
    public int getItemCount() {
        return mRestaurants.size();
    }
}
