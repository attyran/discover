package com.example.discover.viewmodel;

import com.example.discover.model.Restaurant;

/**
 * Restaurant item interface
 */
public interface DetailInteractor {
    void setDetails(Restaurant restaurant);
}
