package com.example.discover.utils;

import com.example.discover.model.DiscoverApi;
import com.example.discover.model.Restaurant;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestaurantServiceManagerTest {
    private DiscoverApi mApi;
    private static final String BASE_URL = "https://api.doordash.com/v2/";

    @Before
    public void setup() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mApi = retrofit.create(DiscoverApi.class);
    }

    @Test
    public void fetchValidNearbyRestaurants() {
        try {
            Call<List<Restaurant>> call = mApi.getNearbyRestaurants(37.422740, -122.139956);
            Response<List<Restaurant>> response = call.execute();
            Assert.assertTrue(response.isSuccessful());
            Assert.assertTrue(response.body().size() > 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void fetchInvalidNearbyRestaurants() {
        try {
            Call<List<Restaurant>> call = mApi.getNearbyRestaurants(0, 0);
            Response<List<Restaurant>> response = call.execute();
            Assert.assertTrue(response.isSuccessful());
            Assert.assertFalse(response.body().size() > 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void fetchValidRestaurant() {
        try {
            Call<Restaurant> call = mApi.getRestaurant(50);
            Response<Restaurant> response = call.execute();
            Assert.assertTrue(response.isSuccessful());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void fetchInvalidRestaurant() {
        try {
            Call<Restaurant> call = mApi.getRestaurant(-50);
            Response<Restaurant> response = call.execute();
            Assert.assertFalse(response.isSuccessful());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
