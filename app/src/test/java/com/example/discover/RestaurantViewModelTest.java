package com.example.discover;

import android.content.Context;

import com.example.discover.model.Restaurant;
import com.example.discover.viewmodel.Interactor;
import com.example.discover.viewmodel.RestaurantViewModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RestaurantViewModelTest {
    private static final List<Restaurant> RESTAURANTS = TestDataGenerator.generateRestaurantList(10);
    private static final List<Restaurant> EMPTY_RESTAURANTS = new ArrayList<>();

    @Mock
    private Context mContext;

    @Mock
    private Interactor interactor;

    @Mock
    private RestaurantViewModel mRestaurantVM;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mRestaurantVM = new RestaurantViewModel(mContext, interactor);
    }

    private void setupContext() {
        when(mContext.getApplicationContext()).thenReturn(mContext);
    }

    @Test
    public void getRestaurantsSuccess() {

    }
}
