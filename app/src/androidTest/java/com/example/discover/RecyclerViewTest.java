package com.example.discover;

import com.example.discover.model.DiscoverApi;
import com.example.discover.model.Restaurant;
import com.example.discover.utils.RestaurantServiceManager;
import com.example.discover.viewmodel.MainActivity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import android.content.Intent;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public class RecyclerViewTest {
    private DiscoverApi mApi;
    private static final String BASE_URL = "https://api.doordash.com/v2/";

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class, true, false);

    @Before
    public void init() {
        Intent intent = new Intent();
        mActivityRule.launchActivity(intent);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mApi = retrofit.create(DiscoverApi.class);
    }

    @Test
    public void testRVShown() {
        onView(withId(R.id.recyclerview_search_results)).check(matches(isDisplayed()));
    }

    @Test
    public void testClick() {
        try {
            Call<List<Restaurant>> call = mApi.getNearbyRestaurants(37.422740, -122.139956);
            Response<List<Restaurant>> response = call.execute();
            Assert.assertTrue(response.isSuccessful());
            Assert.assertTrue(response.body().size() > 1);
            onView(withId(R.id.recyclerview_search_results)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
